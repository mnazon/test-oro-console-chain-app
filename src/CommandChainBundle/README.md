1. Register bundle

```php
<?php

return [
    Symfony\Bundle\FrameworkBundle\FrameworkBundle::class => ['all' => true],
    BarBundle\BarBundle::class =>  ['all' => true],
    FooBundle\FooBundle::class =>  ['all' => true],
    CommandChainBundle\CommandChainBundle::class =>  ['all' => true],
];
```

2. Register command by tag as master command

```yml
services:
    FooBundle\Command\FooCommand:
        tags:
            - { name: 'command_chain.chain_command', master: true }
```

3. Register command by tag as a child of foo

```yml
services:
    BarBundle\Command\BarCommand:
        tags:
          - { name: 'command_chain.chain_command', childOf: 'foo' }
```
