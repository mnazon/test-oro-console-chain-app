<?php

declare(strict_types=1);

namespace CommandChainBundle;

use CommandChainBundle\DependencyInjection\Compiler\CommandChainCollectorPass;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class CommandChainBundle extends Bundle
{
    public function build(ContainerBuilder $container): void
    {
        $container->addCompilerPass(new CommandChainCollectorPass());
    }
}
