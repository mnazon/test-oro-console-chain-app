<?php

declare(strict_types=1);

namespace CommandChainBundle\Tests\Functional;

use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\BufferedOutput;

class CommandChainTest extends KernelTestCase
{
    private const MASTER_COMMAND = 'test:master:run';
    private const CHILD_COMMAND = 'test:child:run';
    private const OUT_OF_CHAIN_COMMAND = 'test:out-of-chain:run';

    private Application $application;

    protected function setUp(): void
    {
        $kernel = static::createKernel();
        $this->application = new Application($kernel);
    }

    public function testRunMaster(): void
    {
        $expectedDisplays = [
            'Hi from master command',
            'Hi from child command',
        ];

        $input = new ArrayInput([self::MASTER_COMMAND]);
        $bufferedOutput = new BufferedOutput();
        $this->application->doRun($input, $bufferedOutput);

        $output = \trim($bufferedOutput->fetch());
        foreach ($expectedDisplays as $display) {
            $this->assertStringContainsString($display, $output);
        }
    }

    public function testRunChild(): void
    {
        $input = new ArrayInput([self::CHILD_COMMAND]);
        $bufferedOutput = new BufferedOutput();
        $this->application->doRun($input, $bufferedOutput);

        $output = \trim($bufferedOutput->fetch());

        $this->assertSame(\sprintf(
            'Error: %s command is a member of command chain and cannot be executed on its own.',
            self::CHILD_COMMAND
        ), $output);
    }

    public function testRunOutOffChain(): void
    {
        $input = new ArrayInput([self::OUT_OF_CHAIN_COMMAND]);
        $bufferedOutput = new BufferedOutput();
        $this->application->doRun($input, $bufferedOutput);

        $output = \trim($bufferedOutput->fetch());

        $this->assertSame('Hi from out-of-chain command', $output);
    }
}
