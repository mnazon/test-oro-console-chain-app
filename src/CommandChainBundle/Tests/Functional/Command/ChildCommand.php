<?php

declare(strict_types=1);

namespace CommandChainBundle\Tests\Functional\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ChildCommand extends Command
{
    protected static $defaultName = 'test:child:run';

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $output->writeln('Hi from child command');

        return Command::SUCCESS;
    }
}
