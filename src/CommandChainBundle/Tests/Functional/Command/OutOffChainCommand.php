<?php

declare(strict_types=1);

namespace CommandChainBundle\Tests\Functional\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class OutOffChainCommand extends Command
{
    protected static $defaultName = 'test:out-of-chain:run';

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $output->writeln('Hi from out-of-chain command');

        return Command::SUCCESS;
    }
}
