<?php

declare(strict_types=1);

namespace CommandChainBundle\Tests\Functional\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class MasterCommand extends Command
{
    protected static $defaultName = 'test:master:run';

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $output->writeln('Hi from master command');

        return Command::SUCCESS;
    }
}
