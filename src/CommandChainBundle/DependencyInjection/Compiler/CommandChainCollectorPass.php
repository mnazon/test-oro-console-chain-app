<?php

declare(strict_types=1);

namespace CommandChainBundle\DependencyInjection\Compiler;

use CommandChainBundle\CommandChain\CommandChainCollector;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\Reference;

class CommandChainCollectorPass implements CompilerPassInterface
{
    private const TAG_NAME = 'command_chain.chain_command';

    public function process(ContainerBuilder $container): void
    {
        $definition = $container->findDefinition(CommandChainCollector::class);
        $taggedServices = $container->findTaggedServiceIds(self::TAG_NAME);

        $this->initChains($definition, $taggedServices);
        $this->addChildren($definition, $taggedServices);
    }

    public function initChains(Definition $collectorDef, array $taggedServices): void
    {
        foreach ($taggedServices as $id => $tags) {
            foreach ($tags as $tag) {
                if (isset($tag['master']) && true === $tag['master']) {
                    $collectorDef->addMethodCall('initChain', [new Reference($id)]);
                    break;
                }
            }
        }
    }

    public function addChildren(Definition $collectorDef, array $taggedServices): void
    {
        foreach ($taggedServices as $id => $tags) {
            foreach ($tags as $tag) {
                if (isset($tag['childOf'])) {
                    $collectorDef->addMethodCall('addToChain', [$tag['childOf'], new Reference($id)]);
                    break;
                }
            }
        }
    }
}
