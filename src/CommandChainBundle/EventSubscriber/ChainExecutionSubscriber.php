<?php

declare(strict_types=1);

namespace CommandChainBundle\EventSubscriber;

use CommandChainBundle\CommandChain\CommandChainCollector;
use CommandChainBundle\CommandChain\CommandChainExecutor;
use Symfony\Component\Console\ConsoleEvents;
use Symfony\Component\Console\Event\ConsoleCommandEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class ChainExecutionSubscriber implements EventSubscriberInterface
{
    public function __construct(
        private CommandChainExecutor  $chainExecutor,
        private CommandChainCollector $chainCollector
    ) {}

    public static function getSubscribedEvents(): array
    {
        return [
            ConsoleEvents::COMMAND => 'handle',
        ];
    }

    public function handle(ConsoleCommandEvent $event): void
    {
        $command = $event->getCommand();
        $commandName = $command->getName();
        $chain = $this->chainCollector->getChain($commandName);
        if ($chain) {
            $this->chainExecutor->addOutput($event->getOutput());
            $this->chainExecutor->execute($chain);

            $event->disableCommand();

            return;
        }

        if ($this->chainCollector->hasChild($commandName)) {
            $event
                ->getOutput()
                ->write(\sprintf(
                    'Error: %s command is a member of command chain and cannot be executed on its own.',
                    $commandName
                ), true);

            $event->disableCommand();
        }
    }
}
