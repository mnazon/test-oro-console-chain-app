<?php

declare(strict_types=1);

namespace CommandChainBundle\CommandChain;

use Symfony\Component\Console\Command\Command;

class CommandChain implements CommandChainInterface
{
    /**
     * @var Command[]
     */
    private array $children = [];

    public function __construct(private Command $masterCommand)
    {}

    public function addChild(Command $command): void
    {
        $name = $command->getName();
        if ($this->hasChild($name)) {
            throw new \LogicException(\sprintf('Command with name %s is already added', $name));
        }

        $this->children[$name] = $command;
    }

    public function hasChild($name): bool
    {
        return \array_key_exists($name, $this->children);
    }

    public function getChildren(): array
    {
        return $this->children;
    }

    public function getMasterCommand(): Command
    {
        return $this->masterCommand;
    }
}
