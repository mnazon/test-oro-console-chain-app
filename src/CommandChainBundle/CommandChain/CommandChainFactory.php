<?php

declare(strict_types=1);

namespace CommandChainBundle\CommandChain;

use Symfony\Component\Console\Command\Command;

class CommandChainFactory implements CommandChainFactoryInterface
{
    public function create(Command $masterCommand): CommandChain
    {
        return new CommandChain($masterCommand);
    }
}
