<?php

declare(strict_types=1);

namespace CommandChainBundle\CommandChain;

use Symfony\Component\Console\Command\Command;

class CommandChainCollector
{
    /**
     * @var CommandChain[]
     */
    private array $chainCollection = [];

    public function __construct(private CommandChainFactoryInterface $chainFactory)
    {}

    public function initChain(Command $masterCommand): void
    {
        $this->chainCollection[$masterCommand->getName()] = $this->chainFactory->create($masterCommand);
    }

    public function addToChain(string $name, Command $command)
    {
        if (!$chain = $this->getChain($name)) {
            throw new \LogicException('Init Chain at first');
        }

        $chain->addChild($command);
    }

    public function getChain(string $name): ?CommandChain
    {
        return $this->chainCollection[$name] ?? null;
    }

    public function hasChild(string $name): bool
    {
        foreach ($this->chainCollection as $chain) {
            if ($chain->hasChild($name)) {
                return true;
            }
        }

        return false;
    }
}
