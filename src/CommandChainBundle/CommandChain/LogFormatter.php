<?php

declare(strict_types=1);

namespace CommandChainBundle\CommandChain;

class LogFormatter
{
    public function __invoke(string $level, string $message, array $context = []): string
    {
        return \sprintf('[%s] %s', date('Y-m-d H:i:s'), $message).\PHP_EOL;
    }
}
