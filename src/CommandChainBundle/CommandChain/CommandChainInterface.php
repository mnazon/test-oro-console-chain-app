<?php

declare(strict_types=1);

namespace CommandChainBundle\CommandChain;

use Symfony\Component\Console\Command\Command;

interface CommandChainInterface
{
    public function getMasterCommand(): Command;

    /**
     * @return Command[]
     */
    public function getChildren(): array;

    public function addChild(Command $command): void;
}
