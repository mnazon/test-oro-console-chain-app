<?php

declare(strict_types=1);

namespace CommandChainBundle\CommandChain;

use CommandChainBundle\CommandChain\Output\ChainOutput;
use CommandChainBundle\CommandChain\Output\LoggedOutput;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\Output;

class CommandChainExecutor
{
    private InputInterface $input;
    private ChainOutput $output;

    private Command $masterCommand;
    /**
     * @var Command[]
     */
    private array $childrenCommands;

    public function __construct(private LoggerInterface $logger)
    {
        $this->input = new ArrayInput([]);
        $this->output = new ChainOutput();

        $this->addOutput(new LoggedOutput($this->logger));
    }

    public function addOutput(Output $output)
    {
        $this->output->add($output);
    }

    public function execute(CommandChainInterface $commandChain): void
    {
        $this->registerMaster($commandChain);
        $this->registerChildren($commandChain);

        $this->runMaster();
        $this->runChildren();

        $this->postRun();
    }

    private function registerMaster(CommandChainInterface $commandChain): void
    {
        $this->masterCommand = $commandChain->getMasterCommand();

        $this->log(\sprintf(
            '%s is a master command of a command chain that has registered member commands',
            $this->masterCommand->getName()
        ));
    }

    private function registerChildren(CommandChainInterface $commandChain): void
    {
        $this->childrenCommands = $commandChain->getChildren();

        foreach ($this->childrenCommands as $command) {
            $this->log(\sprintf(
                '%s registered as a member of %s command chain',
                $command->getName(),
                $this->masterCommand->getName()
            ));
        }
    }

    private function runMaster(): void
    {
        $this->log(\sprintf(
            'Executing %s command itself first:',
            $this->masterCommand->getName()
        ));

        $this->masterCommand->run($this->input, $this->output);
    }

    private function runChildren(): void
    {
        $this->log(\sprintf(
            'Executing %s chain members:',
            $this->masterCommand->getName()
        ));

        foreach ($this->childrenCommands as $command) {
            $command->run($this->input, $this->output);
        }
    }

    private function postRun()
    {
        $this->log(\sprintf(
            'Execution of %s chain completed.',
            $this->masterCommand->getName()
        ));
    }

    private function log(string $msg)
    {
        $this->logger->info($msg);
    }
}
