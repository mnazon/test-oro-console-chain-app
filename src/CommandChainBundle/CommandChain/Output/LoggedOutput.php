<?php

declare(strict_types=1);

namespace CommandChainBundle\CommandChain\Output;

use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Formatter\OutputFormatterInterface;
use Symfony\Component\Console\Output\Output;

class LoggedOutput extends Output
{
    public function __construct(
        private LoggerInterface $logger,
        ?int $verbosity = self::VERBOSITY_NORMAL,
        bool $decorated = false,
        OutputFormatterInterface $formatter = null
    ) {
        parent::__construct($verbosity, $decorated, $formatter);
    }

    protected function doWrite(string $message, bool $newline)
    {
        $this->logger->info($message);
    }
}
