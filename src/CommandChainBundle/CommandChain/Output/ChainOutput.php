<?php

declare(strict_types=1);

namespace CommandChainBundle\CommandChain\Output;

use Symfony\Component\Console\Output\Output;

class ChainOutput extends Output
{
    /**
     * @var Output[]
     */
    private array $outputs = [];

    public function add(Output $output)
    {
        $this->outputs[] = $output;
    }

    protected function doWrite(string $message, bool $newline)
    {
        foreach ($this->outputs as $output) {
            $output->doWrite($message, $newline);
        }
    }
}
