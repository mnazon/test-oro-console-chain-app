<?php

declare(strict_types=1);

namespace CommandChainBundle\CommandChain;

use Symfony\Component\Console\Command\Command;

interface CommandChainFactoryInterface
{
    public function create(Command $masterCommand): CommandChainInterface;
}
