<?php

return [
    Symfony\Bundle\FrameworkBundle\FrameworkBundle::class => ['all' => true],
    BarBundle\BarBundle::class =>  ['all' => true],
    FooBundle\FooBundle::class =>  ['all' => true],
    CommandChainBundle\CommandChainBundle::class =>  ['all' => true],
];
